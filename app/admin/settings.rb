ActiveAdmin.register_page 'Settings' do
  page_action :update, method: :patch do
    settings_params = params.require(:settings).permit(:title, :admin_email)

    if Settings.first.update(settings_params)
      redirect_to admin_settings_path, notice: 'Settings updated'
    else
      redirect_to admin_settings_path, alert: 'Settings not updated'
    end
  end

  content do
    render partial: 'form', locals: { settings: Settings.first }
  end

  controller do
    def settings_params
      params.require(:settings).permit(:title, :admin_email)
    end
  end
end
